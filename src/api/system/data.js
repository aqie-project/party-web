import request from '@/utils/request'

// 查询学习资料列表
export function listData(query) {
  return request({
    url: '/system/data/list',
    method: 'get',
    params: query
  })
}

// 查询学习资料详细
export function getData(learnId) {
  return request({
    url: '/system/data/' + learnId,
    method: 'get'
  })
}

// 新增学习资料
export function addData(data) {
  return request({
    url: '/system/data',
    method: 'post',
    data: data
  })
}

// 修改学习资料
export function updateData(data) {
  return request({
    url: '/system/data',
    method: 'put',
    data: data
  })
}

// 删除学习资料
export function delData(learnId) {
  return request({
    url: '/system/data/' + learnId,
    method: 'delete'
  })
}

// 导出学习资料
export function exportData(query) {
  return request({
    url: '/system/data/export',
    method: 'get',
    params: query
  })
}