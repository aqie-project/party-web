import request from '@/utils/request'

// 查询用户答题记录列表
export function listAnswerHistory(query) {
  return request({
    url: '/system/users/answerHistory',
    method: 'get',
    params: query
  })
}

// 查询用户答题记录详细
export function getAnswer(id) {
  return request({
    url: '/system/answer/' + id,
    method: 'get'
  })
}

// 新增用户答题记录
export function addAnswer(data) {
  return request({
    url: '/system/answer',
    method: 'post',
    data: data
  })
}

// 修改用户答题记录
export function updateAnswer(data) {
  return request({
    url: '/system/users/answerHistory',
    method: 'put',
    data: data
  })
}

// 删除用户答题记录
export function delAnswer(id) {
  return request({
    url: '/system/answer/' + id,
    method: 'delete'
  })
}

// 导出用户答题记录
export function exportAnswerHistory(query) {
  return request({
    url: '/system/users/exportAnswerHistory',
    method: 'get',
    params: query
  })
}
