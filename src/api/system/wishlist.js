import request from '@/utils/request'

// 查询心愿列列表
export function listWishlist(query) {
  return request({
    url: '/system/wishlist/list',
    method: 'get',
    params: query
  })
}

// 查询心愿列详细
export function getWishlist(id) {
  return request({
    url: '/system/wishlist/' + id,
    method: 'get'
  })
}

// 新增心愿列
export function addWishlist(data) {
  return request({
    url: '/system/wishlist',
    method: 'post',
    data: data
  })
}

// 修改心愿列
export function updateWishlist(data) {
  return request({
    url: '/system/wishlist',
    method: 'put',
    data: data
  })
}

// 删除心愿列
export function delWishlist(id) {
  return request({
    url: '/system/wishlist/' + id,
    method: 'delete'
  })
}

// 导出心愿列
export function exportWishlist(query) {
  return request({
    url: '/system/wishlist/export',
    method: 'get',
    params: query
  })
}