import request from '@/utils/request'

// 查询用户学习历史列表
export function listWatch(query) {
  return request({
    url: '/system/watch/list',
    method: 'get',
    params: query
  })
}

// 查询用户学习记录列表
export function recordListWatch(query) {
  return request({
    url: '/system/watch/record/list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getWatch(id) {
  return request({
    url: '/system/watch/' + id,
    method: 'get'
  })
}

// 新增
export function addWatch(data) {
  return request({
    url: '/system/watch',
    method: 'post',
    data: data
  })
}

// 修改
export function updateWatch(data) {
  return request({
    url: '/system/watch',
    method: 'put',
    data: data
  })
}

// 删除
export function delWatch(id) {
  return request({
    url: '/system/watch/' + id,
    method: 'delete'
  })
}

// 导出用户学习历史
export function exportWatch(query) {
  return request({
    url: '/system/watch/export',
    method: 'get',
    params: query
  })
}

// 导出用户学习记录
export function exportWatchRecord(query) {
  return request({
    url: '/system/watch/export/record',
    method: 'get',
    params: query
  })
}
