import request from '@/utils/request'

// 查询楼宇列表
export function listBuilding() {
  return request({
    url: '/system/organization/building/list',
    method: 'get',
  })
}

// 查询两新组织列表
export function listOrganization(query) {
  return request({
    url: '/system/organization/list',
    method: 'get',
    params: query
  })
}

// 查询两新组织详细
export function getOrganization(id) {
  return request({
    url: '/system/organization/' + id,
    method: 'get'
  })
}

// 新增两新组织
export function addOrganization(data) {
  return request({
    url: '/system/organization',
    method: 'post',
    data: data
  })
}

// 修改两新组织
export function updateOrganization(data) {
  return request({
    url: '/system/organization',
    method: 'put',
    data: data
  })
}

// 删除两新组织
export function delOrganization(id) {
  return request({
    url: '/system/organization/' + id,
    method: 'delete'
  })
}

// 导出两新组织
export function exportOrganization(query) {
  return request({
    url: '/system/organization/export',
    method: 'get',
    params: query
  })
}
