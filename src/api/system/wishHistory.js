import request from '@/utils/request'

// 查询用户心愿列表
export function listWishHistory(query) {
  return request({
    url: '/system/wish/userWishHistory',
    method: 'get',
    params: query
  })
}

// 查询用户心愿详细
export function getWish(id) {
  return request({
    url: '/system/wish/' + id,
    method: 'get'
  })
}

// 新增用户心愿
export function addWish(data) {
  return request({
    url: '/system/wish',
    method: 'post',
    data: data
  })
}

// 修改用户心愿
export function updateWish(data) {
  return request({
    url: '/system/wish',
    method: 'put',
    data: data
  })
}

// 删除用户心愿
export function delWish(id) {
  return request({
    url: '/system/wish/' + id,
    method: 'delete'
  })
}

// 导出用户心愿
export function exportWishHistory(query) {
  return request({
    url: '/system/wish/exportUserWishHistory',
    method: 'get',
    params: query
  })
}
