import request from '@/utils/request'

// 查询团支部列表
export function listLeague_branch(query) {
  return request({
    url: '/system/league_branch/list',
    method: 'get',
    params: query
  })
}

// 查询团支部详细
export function getLeague_branch(id) {
  return request({
    url: '/system/league_branch/' + id,
    method: 'get'
  })
}

// 新增团支部
export function addLeague_branch(data) {
  return request({
    url: '/system/league_branch',
    method: 'post',
    data: data
  })
}

// 修改团支部
export function updateLeague_branch(data) {
  return request({
    url: '/system/league_branch',
    method: 'put',
    data: data
  })
}

// 删除团支部
export function delLeague_branch(id) {
  return request({
    url: '/system/league_branch/' + id,
    method: 'delete'
  })
}

// 导出团支部
export function exportLeague_branch(query) {
  return request({
    url: '/system/league_branch/export',
    method: 'get',
    params: query
  })
}