import request from '@/utils/request'

// 查询地图列表
export function listMap(query) {
  return request({
    url: '/system/map/list',
    method: 'get',
    params: query
  })
}

// 查询地图详细
export function getMap(mapId) {
  return request({
    url: '/system/map/' + mapId,
    method: 'get'
  })
}

// 新增地图
export function addMap(data) {
  return request({
    url: '/system/map',
    method: 'post',
    data: data
  })
}

// 修改地图
export function updateMap(data) {
  return request({
    url: '/system/map',
    method: 'put',
    data: data
  })
}

// 删除地图
export function delMap(mapId) {
  return request({
    url: '/system/map/' + mapId,
    method: 'delete'
  })
}

// 导出地图
export function exportMap(query) {
  return request({
    url: '/system/map/export',
    method: 'get',
    params: query
  })
}