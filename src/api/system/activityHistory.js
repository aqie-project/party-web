import request from '@/utils/request'

// 查询用户活动列表
export function listActivityHistory(query) {
  return request({
    url: '/system/wish/userPartyActivityHistory',
    method: 'get',
    params: query
  })
}

// 查询用户活动详细
export function getActivity(id) {
  return request({
    url: '/system/activity/' + id,
    method: 'get'
  })
}

// 新增用户活动
export function addActivity(data) {
  return request({
    url: '/system/activity',
    method: 'post',
    data: data
  })
}

// 修改用户活动
export function updateActivity(data) {
  return request({
    url: '/system/users/activityHistory',
    method: 'put',
    data: data
  })
}

// 删除用户活动
export function delActivity(id) {
  return request({
    url: '/system/activity/' + id,
    method: 'delete'
  })
}

// 导出用户活动
export function exportActivityHistory(query) {
  return request({
    url: '/system/wish/exportActivityHistory',
    method: 'get',
    params: query
  })
}
