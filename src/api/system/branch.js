import request from '@/utils/request'

// 查询党支部列表
export function listBranch(query) {
  return request({
    url: '/system/branch/list',
    method: 'get',
    params: query
  })
}

// 查询党支部详细
export function getBranch(id) {
  return request({
    url: '/system/branch/' + id,
    method: 'get'
  })
}

// 新增党支部
export function addBranch(data) {
  return request({
    url: '/system/branch',
    method: 'post',
    data: data
  })
}

// 修改党支部
export function updateBranch(data) {
  return request({
    url: '/system/branch',
    method: 'put',
    data: data
  })
}

// 删除党支部
export function delBranch(id) {
  return request({
    url: '/system/branch/' + id,
    method: 'delete'
  })
}

// 导出党支部
export function exportBranch(query) {
  return request({
    url: '/system/branch/export',
    method: 'get',
    params: query
  })
}