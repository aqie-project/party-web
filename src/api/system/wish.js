import request from '@/utils/request'

// 查询心愿 活动列表
export function listWish(query) {
  return request({
    url: '/system/wish/list',
    method: 'get',
    params: query
  })
}

// 查询心愿 活动详细
export function getWish(id) {
  return request({
    url: '/system/wish/' + id,
    method: 'get'
  })
}

// 新增心愿 活动
export function addWish(data) {
  return request({
    url: '/system/wish',
    method: 'post',
    data: data
  })
}

// 修改心愿 活动
export function updateWish(data) {
  return request({
    url: '/system/wish',
    method: 'put',
    data: data
  })
}

// 删除心愿 活动
export function delWish(id) {
  return request({
    url: '/system/wish/' + id,
    method: 'delete'
  })
}

// 导出心愿 活动
export function exportWish(query) {
  return request({
    url: '/system/wish/export',
    method: 'get',
    params: query
  })
}