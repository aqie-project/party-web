import request from '@/utils/request'

// 查询视频列表
export function listMedia(query) {
  return request({
    url: '/system/media/list',
    method: 'get',
    params: query
  })
}

// 查询视频详细
export function getMedia(mediaId) {
  return request({
    url: '/system/media/' + mediaId,
    method: 'get'
  })
}

// 新增视频
export function addMedia(data) {
  return request({
    url: '/system/media',
    method: 'post',
    data: data
  })
}

// 修改视频
export function updateMedia(data) {
  return request({
    url: '/system/media',
    method: 'put',
    data: data
  })
}

// 删除视频
export function delMedia(mediaId) {
  return request({
    url: '/system/media/' + mediaId,
    method: 'delete'
  })
}

// 导出视频
export function exportMedia(query) {
  return request({
    url: '/system/media/export',
    method: 'get',
    params: query
  })
}
