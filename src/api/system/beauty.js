import request from '@/utils/request'

// 查询风采展示列表
export function listBeauty(query) {
  return request({
    url: '/system/beauty/list',
    method: 'get',
    params: query
  })
}

// 查询风采展示详细
export function getBeauty(beautyId) {
  return request({
    url: '/system/beauty/' + beautyId,
    method: 'get'
  })
}

// 新增风采展示
export function addBeauty(data) {
  return request({
    url: '/system/beauty',
    method: 'post',
    data: data
  })
}

// 修改风采展示
export function updateBeauty(data) {
  return request({
    url: '/system/beauty',
    method: 'put',
    data: data
  })
}

// 删除风采展示
export function delBeauty(beautyId) {
  return request({
    url: '/system/beauty/' + beautyId,
    method: 'delete'
  })
}

// 导出风采展示
export function exportBeauty(query) {
  return request({
    url: '/system/beauty/export',
    method: 'get',
    params: query
  })
}